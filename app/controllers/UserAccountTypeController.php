<?php

namespace App\Controllers;

use App\Controllers\HttpExceptions\Http400Exception;
use App\Controllers\HttpExceptions\Http500Exception;
use App\Services\ServiceException;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

/**
 * Operations with UserAccountType: CRUD
 */
class UserAccountTypeController extends AbstractController
{
    /**
     * Adding user account type
     */
    public function addAction()
    {
	    /** Init Block **/
        $errors = [];
        $data = [];
	    /** End Init Block **/

	    /** Validation Block **/
        $validation = new Validation();
        $validation->add(
            [
                'user_id',
                'account_type_id',
                'active'
            ],
            new PresenceOf(
                [
                    "message" => [
                        "username" => "The user_id is required",
                        "account_type_id" => "The account_type_id is required",
                        "active" => "The active is required",
                    ],
                ]
            )
        );

        $messages = $validation->validate($_POST);

        if (count($messages)) {
            foreach ($messages as $message) {
                $errors[] = $message->getMessage();
            }
            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }
	    /** End Validation Block **/

        $data['user_id'] = $this->request->getPost('user_id');
        $data['account_type_id'] = $this->request->getPost('account_type_id');
        $data['active'] = $this->request->getPost('active');
        $data['expires_at'] = $this->request->getPost('expires_at');

	    /** Passing to business logic and preparing the response **/
        try {
            $this->userAccountTypeService->createUserAccountType($data);
        } catch (ServiceException $e) {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }
	    /** End Passing to business logic and preparing the response  **/
    }

    /**
     * Returns user account type list
     *
     * @return array
     */
    public function getUserAccountTypeListAction()
    {
        try {
            $userAccountTypeList = $this->userAccountTypeService->getUserAccountTypeList();
        } catch (ServiceException $e) {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $userAccountTypeList;
    }

     /**
     * Updating existing user account type
     *
     * @param string $userId
     * @param string $accountTypeId
     */
    public function updateUserAccountTypeAction($userId, $accountTypeId)
    {
        $errors = [];
        $data   = [];

        /** Validation Block **/
        $validation = new Validation();
        $validation->add(
            [
                'user_id',
                'account_type_id',
                'active'
            ],
            new PresenceOf(
                [
                    "message" => [
                        "username" => "The user_id is required",
                        "account_type_id" => "The account_type_id is required",
                        "active" => "The active is required",
                    ],
                ]
            )
        );

        $messages = $validation->validate($this->request->getPut());

        if (count($messages)) {
            foreach ($messages as $message) {
                $errors[] = $message->getMessage();
            }
            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }
        /** End Validation Block **/

        $data['user_id'] = $this->request->getPut('user_id');
        $data['account_type_id'] = $this->request->getPut('account_type_id');
        $data['active'] = $this->request->getPut('active');
        $data['expires_at'] = $this->request->getPut('expires_at');

        if (!ctype_digit($userId) || ($userId < 0)  || !ctype_digit($accountTypeId) || ($accountTypeId < 0)) {
            $errors['id'] = 'Id must be a positive integer';
        }

        $data['user_id'] = (int)$userId;
        $data['account_type_id'] = (int)$accountTypeId;

        if ($errors) {
            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        try {
            $this->userAccountTypeService->updateUserAccountType($data);
        } catch (ServiceException $e) {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }
    }

    /**
     * Delete an existing user account type
     *
     * @param string $userId
     * @param string $accountTypeId
     */
    public function deleteUserAccountTypeAction($userId, $accountTypeId)
    {

        if (!ctype_digit($userId) || ($userId < 0)) {
            $errors['userId'] = 'Id must be a positive integer';
        }

        if (!ctype_digit($accountTypeId) || ($accountTypeId < 0)) {
            $errors['account_type_id'] = 'Id must be a positive integer';
        }

        try {
        $this->userAccountTypeService->deleteUserAccountType((int)$userId, (int) $accountTypeId);
        } catch (ServiceException $e) {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }
    }
}
