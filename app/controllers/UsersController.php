<?php

namespace App\Controllers;

use App\Controllers\HttpExceptions\Http400Exception;
use App\Controllers\HttpExceptions\Http422Exception;
use App\Controllers\HttpExceptions\Http500Exception;
use App\Services\AbstractService;
use App\Services\ServiceException;
use App\Services\UsersService;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email as EmailValidator;

/**
 * Operations with Users: CRUD
 */
class UsersController extends AbstractController
{
    /**
     * Adding user
     */
    public function addAction()
    {
	    /** Init Block **/
        $errors = [];
        $data = [];
	    /** End Init Block **/

	    /** Validation Block **/
        $validation = new Validation();
        $validation->add(
            [
                'username',
                'password',
                'first_name',
                'last_name',
                'email',
            ],
            new PresenceOf(
                [
                    "message" => [
                        "username" => "The username is required",
                        "password" => "The password is required",
                        "first_name" => "The first_name is required",
                        "last_name" => "The last_name is required",
                        "email" => "The email is required",
                    ],
                ]
            )
        )->add([
            'username',
            'password',
            'first_name',
            'last_name',
        ], new StringLength([
            "max" => [
                "username" => 50,
                "password" => 50,
                "first_name" => 50,
                "last_name" => 50,
            ],
            "min" => [
                "username" => 3,
                "password" => 6,
                "first_name" => 3,
                "last_name" => 3,
            ],
            "messageMaximum" => [
                "username" => "The username must be max 50 letters",
                "password" => "The password must be max 50 letters",
                "first_name" => "The first_name must be max 50 letters",
                "last_name" => "The last_name must be max 50 letters",
            ],
            "messageMinimum" => [
                "username" => "The description must be min 3 letters",
                "password" => "The description must be min 6 letters",
                "first_name" => "The description must be min 3 letters",
                "last_name" => "The description must be min 3 letters",
            ]
        ]));

        $validation->add(
            "email",
            new EmailValidator(
                [
                    "message" => "The e-mail is not valid",
                ]
            )
        );


        $messages = $validation->validate( $_POST );

        if (count($messages)) {
            foreach ($messages as $message) {
                $errors[] = $message->getMessage();
            }

            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }
	    /** End Validation Block **/

        $data['username'] = $this->request->getPost('username');
        $data['password'] = $this->request->getPost('password');
        $data['first_name'] = $this->request->getPost('first_name');
        $data['last_name'] = $this->request->getPost('last_name');
        $data['email'] = $this->request->getPost('email');
        $data['profile_photo'] = $this->request->getPost('profile_photo');
        $data['is_admin'] = $this->request->getPost('is_admin');
        $data['active'] = $this->request->getPost('active');
        $data['restricted'] = $this->request->getPost('restricted');
        $data['company_name'] = $this->request->getPost('company_name');
        $data['country'] = $this->request->getPost('country');
        $data['address_line1'] = $this->request->getPost('address_line1');
        $data['address_line2'] = $this->request->getPost('address_line2');
        $data['city'] = $this->request->getPost('city');
        $data['zipcode'] = $this->request->getPost('zipcode');
        $data['phone_number'] = $this->request->getPost('phone_number');
        $data['payment_email'] = $this->request->getPost('payment_email');
        $data['support_email'] = $this->request->getPost('support_email');
        $data['language'] = $this->request->getPost('language');


        if ($this->request->hasFiles() === true) {
            $data['profile_photo'] = $this->request->getUploadedFiles();
        } else {
            $data['profile_photo'] = null;
        }

	    /** Passing to business logic and preparing the response **/
        try {
            $this->usersService->createUser($data);
        } catch (ServiceException $e) {
            switch ($e->getCode()) {
                case AbstractService::ERROR_ALREADY_EXISTS:
                case UsersService::ERROR_UNABLE_CREATE_USER:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
	    /** End Passing to business logic and preparing the response  **/
    }

    /**
     * Returns user list
     *
     * @return array
     */
    public function getUserListAction()
    {
        try {
            $userList = $this->usersService->getUserList();
        } catch (ServiceException $e) {
            throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
        }

        return $userList;
    }

     /**
     * Updating existing user
     *
     * @param string $userId
     */
    public function updateUserAction($userId)
    {
        $errors = [];
        $data   = [];

        $validation = new Validation();
        $validation->add(
            [
                'username',
                'password',
                'old_password',
                'first_name',
                'last_name',
                'email',
            ],
            new PresenceOf(
                [
                    "message" => [
                        "username" => "The username is required",
                        "password" => "The password is required",
                        "old_password" => "The old_password is required",
                        "first_name" => "The first_name is required",
                        "last_name" => "The last_name is required",
                        "email" => "The email is required",
                    ],
                ]
            )
        )->add([
            'username',
            'password',
            'first_name',
            'last_name',
        ], new StringLength([
            "max" => [

            ],
            "min" => [
                "username" => 3,
                "password" => 6,
                "first_name" => 3,
                "last_name" => 3,
            ],
            "messageMaximum" => [
                "username" => "The username must be max 50 letters",
                "password" => "The password must be max 50 letters",
                "first_name" => "The first_name must be max 50 letters",
                "last_name" => "The last_name must be max 50 letters",
            ],
            "messageMinimum" => [
                "username" => "The description must be min 3 letters",
                "password" => "The description must be min 6 letters",
                "first_name" => "The description must be min 3 letters",
                "last_name" => "The description must be min 3 letters",
            ]
        ]));

        $validation->add(
            "email",
            new EmailValidator(
                [
                    "message" => "The e-mail is not valid",
                ]
            )
        );


        $messages = $validation->validate( $this->request->getPut() );

        if (count($messages)) {
            foreach ($messages as $message) {
                $errors[] = $message->getMessage();
            }
            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }
        /** End Validation Block **/


        $data['username'] = $this->request->getPut('username');
        $data['password'] = $this->request->getPut('password');
        $data['first_name'] = $this->request->getPut('first_name');
        $data['last_name'] = $this->request->getPut('last_name');
        $data['email'] = $this->request->getPut('email');
        $data['profile_photo'] = $this->request->getPut('profile_photo');
        $data['is_admin'] = $this->request->getPut('is_admin');
        $data['active'] = $this->request->getPut('active');
        $data['restricted'] = $this->request->getPut('restricted');
        $data['company_name'] = $this->request->getPut('company_name');
        $data['country'] = $this->request->getPut('country');
        $data['address_line1'] = $this->request->getPut('address_line1');
        $data['address_line2'] = $this->request->getPut('address_line2');
        $data['city'] = $this->request->getPut('city');
        $data['zipcode'] = $this->request->getPut('zipcode');
        $data['phone_number'] = $this->request->getPut('phone_number');
        $data['payment_email'] = $this->request->getPut('payment_email');
        $data['support_email'] = $this->request->getPut('support_email');
        $data['language'] = $this->request->getPut('language');


        if ($this->request->hasFiles() === true) {
            $data['profile_photo'] = $this->request->getUploadedFiles();
        } else {
            $data['profile_photo'] = null;
        }

        if (!ctype_digit($userId) || ($userId < 0)) {
            $errors['id'] = 'Id must be a positive integer';
        }

        $data['id'] = (int)$userId;

        if ($errors) {
            $exception = new Http400Exception(_('Input parameters validation error'), self::ERROR_INVALID_REQUEST);
            throw $exception->addErrorDetails($errors);
        }

        try {
            $this->usersService->updateUser($data);
        } catch (ServiceException $e) {
            switch ($e->getCode()) {
                case UsersService::ERROR_UNABLE_UPDATE_USER:
                case UsersService::ERROR_USER_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }

    /**
     * Delete an existing user
     *
     * @param string $userId
     */
    public function deleteUserAction($userId)
    {
        if (!ctype_digit($userId) || ($userId < 0)) {
            $errors['userId'] = 'Id must be a positive integer';
        }

        try {
            $this->usersService->deleteUser((int)$userId);
        } catch (ServiceException $e) {
            switch ($e->getCode()) {
                case UsersService::ERROR_UNABLE_DELETE_USER:
                case UsersService::ERROR_USER_NOT_FOUND:
                    throw new Http422Exception($e->getMessage(), $e->getCode(), $e);
                default:
                    throw new Http500Exception(_('Internal Server Error'), $e->getCode(), $e);
            }
        }
    }
}
