<?php

namespace App\Services;

use App\Models\Users;

/**
 * Business-logic for users
 *
 * Class UsersService
 */
class UsersService extends AbstractService
{
	/** Unable to create user */
	const ERROR_UNABLE_CREATE_USER = 11001;

	/** User not found */
	const ERROR_USER_NOT_FOUND = 11002;

	/** No such user */
	const ERROR_INCORRECT_USER = 11003;

	/** Unable to update user */
	const ERROR_UNABLE_UPDATE_USER = 11004;

	/** Unable to delete user */
	const ERROR_UNABLE_DELETE_USER = 1105;

	/**
	 * Creating a new user
	 *
	 * @param array $userData
	 */
	public function createUser(array $userData)
	{
		try {
			$user   = new Users();
            $result = $user->setUsername($userData['username'])
                ->setPassword(password_hash($userData['password'], PASSWORD_DEFAULT))
                ->setFirstName($userData['first_name'])
                ->setLastName($userData['last_name'])
                ->setEmail($userData['email'])
                ->setIsAdmin($userData['is_admin'])
                ->setActive($userData['active'])
                ->setRestricted($userData['restricted'])
                ->setCompanyName($userData['company_name'])
                ->setCountry($userData['country'])
                ->setAddressLine1($userData['address_line1'])
                ->setAddressLine2($userData['address_line2'])
                ->setCity($userData['city'])
                ->setZipcode($userData['zipcode'])
                ->setPhoneNumber($userData['phone_number'])
                ->setPaymentEmail($userData['payment_email'])
                ->setSupportEmail($userData['support_email'])
                ->setLanguage($userData['language'])
                ->create();

			if ($userData['profile_photo']){
                $files = $userData['profile_photo'];
                $upload_dir = BASE_PATH . '/public/uploads/profile';
                $dir = $upload_dir . '/' . $user->getId();
                if (!is_dir($dir)) {
                    mkdir($dir, 0755, true);
                }
                #do a loop to handle each file individually
                foreach ($files as $file) {
                    #define a “unique” name and a path to where our file must go
                    $fileName = $file->getName();
                    $fileName = str_replace('(', '_', $fileName);
                    $fileName = str_replace(')', '_', $fileName);
                    $fileName = str_replace(' ', '_', $fileName);
                    $path = $dir . '/' . md5(uniqid(rand(), true)) . '-file-' . strtolower($fileName);
                    #move the file and simultaneously check if everything was ok
                    if ($file->moveTo($path)) {
                        chmod($path, 0755);
                        $result['success'][] = str_replace(BASE_PATH . '/public', '', $path);
                        return Users::findFirst($user->getId())->update([
                            'profile_photo' => $dir
                        ]);
                    } else {
                        $result['errors'][] = $path;
                    }
                }
            }


			if (!$result) {
				throw new ServiceException('Unable to create user', self::ERROR_UNABLE_CREATE_USER);
			}

		} catch (\PDOException $e) {
			if ($e->getCode() == 23505) {
				throw new ServiceException('User already exists', self::ERROR_ALREADY_EXISTS, $e);
			} else {
				throw new ServiceException($e->getMessage(), $e->getCode(), $e);
			}
		}
	}

	/**
	 * Updating an existing user
	 *
	 * @param array $userData
	 */
	public function updateUser(array $userData)
	{
		try {
			$user = Users::findFirst(
				[
					'conditions' => 'id = :id:',
					'bind'       => [
						'id' => $userData['id']
					]
				]
			);

			$userData['username']      = (isset($userData['username'])) ? $user->getUsername() : $userData['username'];
			$userData['password']   = (isset($userData['password'])) ? $user->getPassword() : password_hash($userData['password'], PASSWORD_DEFAULT);
			$userData['first_name'] = (isset($userData['first_name'])) ? $user->getFirstName() : $userData['first_name'];
			$userData['last_name']  = (isset($userData['last_name'])) ? $user->getLastName() : $userData['last_name'];
			$userData['email']  = (isset($userData['email'])) ? $user->getEmail() : $userData['email'];
			$userData['is_admin']  = (isset($userData['is_admin'])) ? $user->getIsAdmin() : $userData['is_admin'];
			$userData['active']  = (isset($userData['active'])) ? $user->getActive() : $userData['active'];
			$userData['restricted']  = (isset($userData['restricted'])) ? $user->getRestricted() : $userData['restricted'];
			$userData['company_name']  = (isset($userData['company_name'])) ? $user->getCompanyName() : $userData['company_name'];
			$userData['country']  = (isset($userData['country'])) ? $user->getCountry() : $userData['country'];
			$userData['address_line1']  = (isset($userData['address_line1'])) ? $user->getAddressLine1() : $userData['address_line1'];
			$userData['address_line2']  = (isset($userData['address_line2'])) ? $user->getAddressLine2() : $userData['address_line2'];
			$userData['city']  = (isset($userData['city'])) ? $user->getCity() : $userData['city'];
			$userData['zipcode']  = (isset($userData['zipcode'])) ? $user->getZipcode() : $userData['zipcode'];
			$userData['phone_number']  = (isset($userData['phone_number'])) ? $user->getPhoneNumber() : $userData['phone_number'];
			$userData['payment_email']  = (isset($userData['payment_email'])) ? $user->getPaymentEmail() : $userData['payment_email'];
			$userData['support_email']  = (isset($userData['support_email'])) ? $user->getSupportEmail() : $userData['support_email'];
			$userData['language']  = (isset($userData['language'])) ? $user->getLanguage() : $userData['language'];

            $result = $user->setUsername($userData['username'])
                ->setPassword(password_hash($userData['password'], PASSWORD_DEFAULT))
                ->setFirstName($userData['first_name'])
                ->setLastName($userData['last_name'])
                ->setEmail($userData['email'])
                ->setIsAdmin($userData['is_admin'])
                ->setActive($userData['active'])
                ->setRestricted($userData['restricted'])
                ->setCompanyName($userData['company_name'])
                ->setCountry($userData['country'])
                ->setAddressLine1($userData['address_line1'])
                ->setAddressLine2($userData['address_line2'])
                ->setCity($userData['city'])
                ->setZipcode($userData['zipcode'])
                ->setPhoneNumber($userData['phone_number'])
                ->setPaymentEmail($userData['payment_email'])
                ->setSupportEmail($userData['support_email'])
                ->setLanguage($userData['language'])
                ->setUpdatedAt(date('Y-m-d H:i:s'))
                ->update();

            if ($userData['profile_photo']){
                $files = $userData['profile_photo'];
                $upload_dir = BASE_PATH . '/public/uploads/profile';
                $dir = $upload_dir . '/' . $user->getId();
                if (!is_dir($dir)) {
                    mkdir($dir, 0755, true);
                }
                #do a loop to handle each file individually
                foreach ($files as $file) {
                    #define a “unique” name and a path to where our file must go
                    $fileName = $file->getName();
                    $fileName = str_replace('(', '_', $fileName);
                    $fileName = str_replace(')', '_', $fileName);
                    $fileName = str_replace(' ', '_', $fileName);
                    $path = $dir . '/' . md5(uniqid(rand(), true)) . '-file-' . strtolower($fileName);
                    #move the file and simultaneously check if everything was ok
                    if ($file->moveTo($path)) {
                        chmod($path, 0755);
                        $result['success'][] = str_replace(BASE_PATH . '/public', '', $path);
                        return Users::findFirst($user->getId())->update([
                            'profile_photo' => $dir
                        ]);
                    } else {
                        $result['errors'][] = $path;
                    }
                }
            }

			if (!$result) {
				throw new ServiceException('Unable to update user', self::ERROR_UNABLE_UPDATE_USER);
			}

		} catch (\PDOException $e) {
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	/**
	 * Delete an existing user
	 *
	 * @param int $userId
	 */
	public function deleteUser($userId)
	{
		try {
			$user = Users::findFirst(
				[
					'conditions' => 'id = :id:',
					'bind'       => [
						'id' => $userId
					]
				]
			);

			if (!$user) {
				throw new ServiceException("User not found", self::ERROR_USER_NOT_FOUND);
			}

			$result = $user->delete();

			if (!$result) {
				throw new ServiceException('Unable to delete user', self::ERROR_UNABLE_DELETE_USER);
			}

		} catch (\PDOException $e) {
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	/**
	 * Returns user list
	 *
	 * @return array
	 */
	public function getUserList()
	{
		try {
			$users = Users::find(
                [
                    'conditions' => '',
                    'bind' => [],
                    'columns' => "id,
                                  username,	 
                                  password,	  
                                  first_name,	  
                                  last_name,
                                  email,	  
                                  profile_photo,	  
                                  is_admin,	  
                                  active,	
                                  restricted,
                                  created_at,	     
                                  updated_at, 
                                  company_name,	     
                                  country,  
                                  address_line1,	     
                                  address_line2,  
                                  city,	   
                                  zipcode,	     
                                  phone_number,	   	     
                                  payment_email,			 
                                  support_email,					        
                                  language",
                ]
            )->toArray();

			if (!$users) {
				return [];
			}

			return $users;
		} catch (\PDOException $e) {
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}
}
