<?php

namespace App\Services;

use App\Models\UserAccountType;

/**
 * Business-logic for user account type
 *
 * Class UserAccountTypeService
 */
class UserAccountTypeService extends AbstractService
{
    /** Unable to create user */
    const ERROR_UNABLE_CREATE_USER_ACCOUNT_TYPE = 11001;

    /** User not found */
    const ERROR_USER_ACCOUNT_TYPE_NOT_FOUND = 11002;

    /** Unable to update user */
    const ERROR_UNABLE_UPDATE_USER_ACCOUNT_TYPE = 11004;

    /** Unable to delete user */
    const ERROR_UNABLE_DELETE_USER_ACCOUNT_TYPE = 1105;
	/**
	 * Creating a new user
	 *
	 * @param array $userData
	 */
	public function createUserAccountType(array $userData)
	{
		try {
			$userAccountType   = new UserAccountType();
            $result = $userAccountType->setUserId($userData['user_id'])
                ->setAccountTypeId($userData['account_type_id'])
                ->setActive($userData['active'])
                ->setExpiresAt($userData['expires_at'])
                ->create();

			if (!$result) {
				throw new ServiceException('Unable to create user account type', self::ERROR_UNABLE_CREATE_USER_ACCOUNT_TYPE);
			}

		} catch (\PDOException $e) {
            throw new ServiceException($e->getMessage(), $e->getCode(), $e);

        }
	}

	/**
	 * Updating an existing user
	 *
	 * @param array $userData
	 */
	public function updateUserAccountType(array $userData)
	{
		try {
            $userAccountType = UserAccountType::findFirst(
				[
					'conditions' => 'user_id = :user_id: AND account_type_id = :account_type_id:',
					'bind'       => [
						'user_id' => $userData['user_id'],
						'account_type_id' => $userData['account_type_id'],
					]
				]
			);

			$userData['user_id']  = (isset($userData['user_id'])) ? $userAccountType->getUserId() : $userData['user_id'];
			$userData['account_type_id']  = (isset($userData['account_type_id'])) ? $userAccountType->getAccountTypeId() : $userData['account_type_id'];
			$userData['active']  = (isset($userData['active'])) ? $userAccountType->getActive() : $userData['active'];
			$userData['expires_at']  = (isset($userData['expires_at'])) ? $userAccountType->getExpiresAt() : $userData['expires_at'];

            $result = $userAccountType->setUserId($userData['user_id'])
                ->setAccountTypeId($userData['account_type_id'])
                ->setActive($userData['active'])
                ->setExpiresAt($userData['expires_at'])
                ->update();

			if (!$result) {
				throw new ServiceException('Unable to update user account type', self::ERROR_UNABLE_UPDATE_USER_ACCOUNT_TYPE);
			}

		} catch (\PDOException $e) {
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	/**
	 * Delete an existing user
	 *
	 * @param int $userId
	 * @param int $accountTypeId
	 */
	public function deleteUserAccountType($userId, $accountTypeId)
	{
		try {
			$userAccountType = UserAccountType::findFirst(
				[
					'conditions' => 'user_id = :user_id: AND account_type_id = :account_type_id:',
					'bind'       => [
						'user_id' => $userId,
						'account_type_id' => $accountTypeId,
					]
				]
			);

			if (!$userAccountType) {
				throw new ServiceException("User account type not found", self::ERROR_USER_ACCOUNT_TYPE_NOT_FOUND);
			}

			$result = $userAccountType->delete();

			if (!$result) {
				throw new ServiceException('Unable to delete user account type', self::ERROR_UNABLE_DELETE_USER_ACCOUNT_TYPE);
			}

		} catch (\PDOException $e) {
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}

	/**
	 * Returns user list
	 *
	 * @return array
	 */
	public function getUserAccountTypeList()
	{
		try {
			$userAccountTypes = UserAccountType::find(
                [
                    'conditions' => '',
                    'bind' => [],
                    'columns' => "user_id,
                                  account_type_id,	 
                                  active,	  
                                  created_at,	  
                                  expires_at"

                ]
            )->toArray();

			if (!$userAccountTypes) {
				return [];
			}

			return $userAccountTypes;
		} catch (\PDOException $e) {
			throw new ServiceException($e->getMessage(), $e->getCode(), $e);
		}
	}
}
