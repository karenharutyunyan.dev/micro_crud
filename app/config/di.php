<?php

use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
use HTMLPurifier;



// Initializing a DI Container
$di = new \Phalcon\DI\FactoryDefault();

/**
 * Overriding Response-object to set the Content-type header globally
 */
$di->setShared(
  'response',
  function () {
      $response = new \Phalcon\Http\Response();
      $response->setContentType('application/json', 'utf-8');

      return $response;
  }
);

//$di->setShared('dispatcher',
//    function () {
//        $eventsManager = new Manager();
//        // Formatting actions before execute
//        $eventsManager->attach(
//            'dispatch:beforeDispatchLoop',
//            function () {
//                $purifier = new HTMLPurifier();
//                $keyParams = [];
//                // Explode each parameter as key,value pairs
//                foreach ($_POST as $key => $value) {
//                    if (is_string($value)) {
//                        $keyParams[$key] = $purifier->purify($value);
//                    } else {
//                        $keyParams[$key] = $value;
//                    }
//                }
//                $_POST = $keyParams;
//            }
//        );
//
//        $dispatcher = new Dispatcher();
//        //Bind the EventsManager to the dispatcher
//        $dispatcher->setEventsManager($eventsManager);
//        return $dispatcher;
//    });

/** Common config */
$di->setShared('config', $config);

/** Database */
$di->set(
  "db",
  function () use ($config) {
      return new MysqlAdapter(
        [
          "host"     => $config->database->host,
          "username" => $config->database->username,
          "password" => $config->database->password,
          "dbname"   => $config->database->dbname,
        ]
      );
  }
);

/** Service to perform operations with the Users */
$di->setShared('usersService', '\App\Services\UsersService');
/** Service to perform operations with the UserAccountType */
$di->setShared('userAccountTypeService', '\App\Services\UserAccountTypeService');

return $di;