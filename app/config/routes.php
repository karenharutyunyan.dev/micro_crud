<?php

$usersCollection = new \Phalcon\Mvc\Micro\Collection();
$usersCollection->setHandler('\App\Controllers\UsersController', true);
$usersCollection->setPrefix('/user');
$usersCollection->post('/add', 'addAction');
$usersCollection->get('/list', 'getUserListAction');
$usersCollection->put('/{userId:[1-9][0-9]*}', 'updateUserAction');
$usersCollection->delete('/{userId:[1-9][0-9]*}', 'deleteUserAction');
$app->mount($usersCollection);

$usersAccountTypeCollection = new \Phalcon\Mvc\Micro\Collection();
$usersAccountTypeCollection->setHandler('\App\Controllers\UserAccountTypeController', true);
$usersAccountTypeCollection->setPrefix('/userAccountType');
$usersAccountTypeCollection->post('/add', 'addAction');
$usersAccountTypeCollection->get('/list', 'getUserAccountTypeListAction');
$usersAccountTypeCollection->put('/{userId:[1-9][0-9]*}/{accountTypeId:[1-9][0-9]*}', 'updateUserAccountTypeAction');
$usersAccountTypeCollection->delete('/{userId:[1-9][0-9]*}/{accountTypeId:[1-9][0-9]*}', 'deleteUserAccountTypeAction');
$app->mount($usersAccountTypeCollection);
// not found URLs
$app->notFound(
  function () use ($app) {
      $exception =
        new \App\Controllers\HttpExceptions\Http404Exception(
          _('URI not found or error in request.'),
          \App\Controllers\AbstractController::ERROR_NOT_FOUND,
          new \Exception('URI not found: ' . $app->request->getMethod() . ' ' . $app->request->getURI())
        );
      throw $exception;
  }
);
