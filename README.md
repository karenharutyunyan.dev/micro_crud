Running migration(s)
    ./vendor/phalcon/devtools/phalcon.sh migration run --log-in-db --ts-based        
Generate migration    
    ./vendor/phalcon/devtools/phalcon.sh migration generate --descr=s

Change your config file
DATABASE_HOST=localhost
DATABASE_USER=root
DATABASE_PASSWORD=pass
DATABASE_DBNAME=test2

composer install
